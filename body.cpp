#include "body.hpp"

namespace nbodies {

Body::Body(float* variables, int dimensions) {
  mass = variables[0];
  radius = variables[1];
  position = std::vector<float>(variables + 2, variables + dimensions + 2);
  velocity = std::vector<float>(variables + dimensions + 2, variables + 2 * dimensions + 2);
  acceleration = std::vector<float>(variables + 2 * dimensions + 2, variables + 3 * dimensions + 2);
}

int Body::byteSize() const {
  return (2 + position.size() + acceleration.size() + velocity.size()) * sizeof(float);
}

std::vector<float> Body::toVector() const {
  std::vector<float> bodyVector;
  bodyVector.push_back(mass);
  bodyVector.push_back(radius);
  bodyVector.insert(bodyVector.end(), position.begin(), position.end());
  bodyVector.insert(bodyVector.end(), velocity.begin(), velocity.end());
  bodyVector.insert(bodyVector.end(), acceleration.begin(), acceleration.end());
  return bodyVector;
}

std::ostream& operator << (std::ostream& out, const Body& body) {
  out << "\033[1;34mMass\033[0m: \033[1;35m" << body.mass << "\033[0m, ";

  out << "\033[1;34mRadius\033[0m: \033[1;35m" << body.radius << "\033[0m, ";

  out << "\033[1;34mPosition\033[0m: (";
  if (body.position.size() > 0) {
    out << "\033[1;35m" << body.position[0] << "\033[0m";
  }
  for (int i = 1; i < body.position.size(); i++) {
    out << ", \033[1;35m" << body.position[i] << "\033[0m"; 
  }

  out << "), " << "\033[1;34mAcceleration\033[0m: (";
  if (body.acceleration.size() > 0) {
    out << "\033[1;35m" << body.acceleration[0] << "\033[0m";
  }
  for (int i = 1; i < body.acceleration.size(); i++) {
    out << ", \033[1;35m" << body.acceleration[i] << "\033[0m";
  }

  out << "), " << "\033[1;34mVelocity\033[0m: (";
  if (body.velocity.size() > 0) {
    out << "\033[1;35m" << body.velocity[0] << "\033[0m";
  }
  for (int i = 1; i < body.velocity.size(); i++) {
    out << ", \033[1;35m" << body.velocity[i] << "\033[0m";
  }
  out << ")";
  return out;
}

} // namespace nbodies