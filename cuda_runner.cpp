#include "cuda_runner.hpp"

namespace nbodies {

CudaRunner::CudaRunner(const std::vector<Body>& bodies, int dimensions) {
  failed = false;

  cuInit(0);
  CUresult res = cuDeviceGet(&cuDevice, 0);
  if (res != CUDA_SUCCESS){
    failed = true;
    return;
  }

  res = cuCtxCreate(&cuContext, 0, cuDevice);
  if (res != CUDA_SUCCESS){
    failed = true;
    return;
  }  

  res = cuModuleLoad(&cuModule, "nbodies.ptx");
  if (res != CUDA_SUCCESS) {
    failed = true;
    return;
  }

  CUfunction* cufunctions[] = {
    &cuUpdateAcceleration, 
    &cuUpdatePosition, 
    &cuUpdateVelocity, 
    &cuClearAcceleration, 
    &cuFindCloseBodies, 
    &cuMergeBodies
  };
  std::string functionNames[] = {
    "updateAcceleration",
    "updatePosition",
    "updateVelocity",
    "clearAcceleration",
    "findCloseBodies",
    "mergeBodies"
  };
  for (int i = 0; i < 6; ++i) {
    res = cuModuleGetFunction(cufunctions[i], cuModule, functionNames[i].c_str());
    if (res != CUDA_SUCCESS) {
      failed = true;
      return;
    }
  }

  Body const* bodiesPtr = &bodies[0];
  res = cuMemAlloc(&cuBodies, bodies.size() * Body::byteSize(dimensions));
  if (res != CUDA_SUCCESS) {
    failed = true;
    return;
  }

  std::vector<float> bodiesRawData;
  for (int i = 0; i < bodies.size(); ++i) {
    std::vector<float> bodyVector = bodiesPtr[i].toVector();
    bodiesRawData.insert(bodiesRawData.end(), bodyVector.begin(), bodyVector.end());
  }
  res = cuMemcpyHtoD(cuBodies, &bodiesRawData[0], bodies.size() * Body::byteSize(dimensions));
  if (res != CUDA_SUCCESS) {
    failed = true;
  }
  res = cuMemAlloc(&cuCandidates, bodies.size() * sizeof(int));
  if (res != CUDA_SUCCESS) {
    throw 0;
  }
}

CudaRunner::~CudaRunner() {
  cuMemFree(cuBodies);
}

std::vector<Body> CudaRunner::simulate(int& bodiesNr, float tick, int dimensions) {
  const int THREADS = 32;
  int blocks = (bodiesNr + THREADS - 1) / THREADS;
  void* args[] = {&cuBodies, &bodiesNr, &dimensions};
  CUresult res;
  // Set acceleration to 0 so that it is not counted multiple times later on.
  res = cuLaunchKernel(cuClearAcceleration, blocks, 1, 1,
             THREADS, 1, 1,
             0, 0, args, 0);
  // Count bodies attraction.
  res = cuLaunchKernel(cuUpdateAcceleration, blocks, 1, 1,
             THREADS, 1, 1,
             0, 0, args, 0);
  if (res != CUDA_SUCCESS) {
    throw 0;
  }
  void* args2[] = {&cuBodies, &bodiesNr, &tick, &dimensions};
  // Count updated velocity based on acceleration.
  res = cuLaunchKernel(cuUpdateVelocity, blocks, 1, 1,
             THREADS, 1, 1,
             0, 0, args2, 0);
  if (res != CUDA_SUCCESS) {
    throw 0;
  }
  // Count updated position based on velocity.
  res = cuLaunchKernel(cuUpdatePosition, blocks, 1, 1,
             THREADS, 1, 1,
             0, 0, args2, 0);
  if (res != CUDA_SUCCESS) {
    throw 0;
  }

  void *args3[] = {&cuBodies, &bodiesNr, &dimensions, &cuCandidates};
  // Look for bodies close to each other.
  res = cuLaunchKernel(cuFindCloseBodies, blocks, 1, 1,
             THREADS, 1, 1,
             0, 0, args3, 0);
  if (res != CUDA_SUCCESS) {
    throw 0;
  }
  // Merge bodies close to each other.
  res = cuLaunchKernel(cuMergeBodies, blocks, 1, 1,
             THREADS, 1, 1,
             0, 0, args3, 0);
  if (res != CUDA_SUCCESS) {
    throw 0;
  }
  float bodiesRawData[bodiesNr * Body::byteSize(dimensions)];
  res = cuMemcpyDtoH(bodiesRawData, cuBodies, bodiesNr * Body::byteSize(dimensions));
  if (res != CUDA_SUCCESS) {
    throw 0;
  }
  std::vector<Body> result;
  int step = 2 + 3 * dimensions;
  int bodyNr = 0, offset = 0;
  // Remove bodies that no longer exist and save to results those who do.
  while (bodyNr < bodiesNr) {
    if (*(bodiesRawData + offset) <= 0.000001) {
      // Swap with the last body.
      for (int i = 0; i < step; ++i) {
        std::swap(*(bodiesRawData + offset + i), *(bodiesRawData + (bodiesNr - 1) * step + i));
      }
      bodiesNr--;
    } else {
      // Add body to result list.
      result.push_back(Body(bodiesRawData + offset, dimensions));
      bodyNr++;
      offset += step;
    }
  }
  res = cuMemcpyHtoD(cuBodies, bodiesRawData, bodiesNr * Body::byteSize(dimensions));
  if (res != CUDA_SUCCESS) {
    throw 0;
  }
  return result;
}
  
} // namespace nbodies