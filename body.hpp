#ifndef _MP_NBODIES_BODY_HPP
#define _MP_NBODIES_BODY_HPP

#include <iostream>
#include <vector>

namespace nbodies {

struct Body {
  float mass;
  float radius;
  std::vector<float> position;
  std::vector<float> acceleration;
  std::vector<float> velocity;
  
  Body(float*, int);

  int byteSize() const;
  std::vector<float> toVector() const;

  static Body read(std::istream& stream, int dimensions) {
    float input[2 + 3 * dimensions];
    for (int i = 0; i < 2 + 2 * dimensions; i++) {
      stream >> input[i];
    }
    for (int i = 2 + 2 * dimensions; i < 2 + 3 * dimensions; i++) {
      input[i] = 0;
    }
    return Body(input, dimensions);
  }

  static int byteSize(int dimensions) {
    return (2 + 3 * dimensions) * sizeof(float);
  }
};

std::ostream& operator << (std::ostream&, const Body&);

} // namespace nbodies

#endif //_MP_NBODIES_BODY_HPP
