#ifndef _MP_NBODIES_CUDA_RUNNER_HPP
#define _MP_NBODIES_CUDA_RUNNER_HPP

#include "body.hpp"
#include "cuda.h"

#include <vector>

namespace nbodies {

class CudaRunner {
  CUcontext cuContext;
  CUdevice cuDevice;
  CUdeviceptr cuBodies;
  CUdeviceptr cuCandidates;
  CUfunction cuClearAcceleration;
  CUfunction cuFindCloseBodies;
  CUfunction cuMergeBodies;
  CUfunction cuUpdateAcceleration;
  CUfunction cuUpdateVelocity;
  CUfunction cuUpdatePosition;
  CUmodule cuModule;

public:
  bool failed;

  CudaRunner(const std::vector<Body>&, int);
  ~CudaRunner();

  std::vector<Body> simulate(int&, float, int);
};

} // namespace nbodies

#endif //_MP_NBODIES_CUDA_RUNNER_HPP
