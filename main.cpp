#include "body.hpp"
#include "cuda_runner.hpp"

#include <iostream>
#include <vector>

int main(int argc, char* argv[]) {
  // Load number of dimensions
  int dimensions;
  std::cout << "\033[1;33mNumber of dimensions:\033[0m ";
  std::cin >> dimensions;

  // Load number of bodies
  int bodiesNr;
  std::cout << "\033[1;33mNumber of bodies:\033[0m ";
  std::cin >> bodiesNr;

  // Load bodies
  std::vector<nbodies::Body> bodies;
  std::cout << "\033[1;33mDescribe bodies (mass, radius, position, velocity, for position and velocity pass as many arguments as is the number of dimensions):\033[0m" << std::endl;
  for (int i = 0; i < bodiesNr; i++) {
    bodies.push_back(nbodies::Body::read(std::cin, dimensions));
  }
  float simulationTime, tick;
  std::cout << "\033[1;33mSimulation time (seconds):\033[0m ";
  std::cin >> simulationTime;
  
  std::cout << "\033[1;33mCalculate every (seconds):\033[0m ";
  std::cin >> tick;

  std::cout << "\033[1;37mInput data:\033[0m" << std::endl;
  for (const auto& body : bodies) {
    std::cout << std::fixed << body << std::endl; 
  }
  // Initialize CUDA
  nbodies::CudaRunner runner(bodies, dimensions);
  if (runner.failed) {
    std::cout << "\033[1;31mFailed to initialize CUDA\033[0m" << std::endl;
    exit(1);
  }
  for (float i = 0; i < simulationTime; i += tick) {
    auto result = runner.simulate(bodiesNr, tick, dimensions);
    std::cout << "\033[1;33mAfter " << i << " seconds:\033[0m" << std::endl;
    for (const auto& body : result) {
      std::cout << std::fixed << body << std::endl; 
    }
  }
  std::cout << "\033[1;32mSimulation finished.\033[0m" << std::endl;
  return 0;
}