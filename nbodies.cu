#include <cmath>

extern "C" {
  // Utility functions:

  __device__ inline float* body(float* bodies, int dimensions, int bodyNr) {
    return bodies + bodyNr * (2 + dimensions * 3);
  }

  __device__ inline float& mass(float* bodies) {
    return *bodies;
  }

  __device__ inline float& radius(float* bodies) {
    return *(bodies + 1);
  }

  __device__ inline float* position(float* bodies) {
    return bodies + 2;
  }

  __device__ inline float* velocity(float* bodies, int dimensions) {
    return bodies + dimensions + 2;
  }

  __device__ inline float* acceleration(float* bodies, int dimensions) {
    return bodies + 2 * dimensions + 2;
  }

  // Simulation functions:

  __global__ void updateAcceleration(float* bodies, int bodiesNr, int dimentions) {
    int bodyNr = blockIdx.x * blockDim.x + threadIdx.x;
    if (bodyNr >= bodiesNr) {
      return;
    }

    float *currentBody = body(bodies, dimentions, bodyNr);
    for (int i = 0; i < bodiesNr; ++i) {
      float *interactingBody = body(bodies, dimentions, i);
      float distance = 0.000001;
      for (int j = 0; j < dimentions; ++j) {
        float singleDistance = position(interactingBody)[j] - position(currentBody)[j];
        distance += singleDistance * singleDistance;
      }

      distance = distance * distance * distance;
      distance = float(1) / sqrtf(distance);
      distance = mass(interactingBody) * distance;

      for (int j = 0; j < dimentions; ++j) {
        acceleration(currentBody, dimentions)[j] += (position(interactingBody)[j] - position(currentBody)[j]) * distance;
      }
    }
  }

  __global__ void updateVelocity(float* bodies, int bodiesNr, float tick, int dimentions) {
    int bodyNr = blockIdx.x * blockDim.x + threadIdx.x;
    if (bodyNr >= bodiesNr) {
      return;
    }

    for (int i = 0; i < dimentions; ++i) {
      velocity(body(bodies, dimentions, bodyNr), dimentions)[i] += acceleration(body(bodies, dimentions, bodyNr), dimentions)[i] * tick;
    }
  }

  __global__ void updatePosition(float* bodies, int bodiesNr, float tick, int dimentions) {
    int bodyNr = blockIdx.x * blockDim.x + threadIdx.x;
    if (bodyNr >= bodiesNr) {
      return;
    }

    for (int i = 0; i < dimentions; ++i) {
      position(body(bodies, dimentions, bodyNr))[i] += velocity(body(bodies, dimentions, bodyNr), dimentions)[i] * tick;
    }
  }

  __global__ void clearAcceleration(float* bodies, int bodiesNr, int dimentions) {
    int bodyNr = blockIdx.x * blockDim.x + threadIdx.x;
    if (bodyNr >= bodiesNr) {
      return;
    }

    for (int i = 0; i < dimentions; ++i) {
      acceleration(body(bodies, dimentions, bodyNr), dimentions)[i] = 0;
    }
  }

  __device__ bool isCloseEnough(float* currentBody, float* interactingBody, int dimentions) {
    float distance = 0;
    for (int i = 0; i < dimentions; ++i) {
      float singleDistance = position(interactingBody)[i] - position(currentBody)[i];
      distance += singleDistance * singleDistance;
    }

    float combinedRadius = radius(interactingBody) + radius(currentBody);
    return distance < combinedRadius * combinedRadius;
  }

  __global__ void findCloseBodies(float* bodies, int bodiesNr, int dimentions, int *close) {
    int bodyNr = blockIdx.x * blockDim.x + threadIdx.x;
    if (bodyNr >= bodiesNr) {
      return;
    }

    close[bodyNr] = bodyNr;
    for (int i = 0; i < bodyNr; ++i) {
      if (isCloseEnough(body(bodies, dimentions, i), body(bodies, dimentions, bodyNr), dimentions)) {
        close[bodyNr] = i;
        break;
      }
    }
  }

  __device__ void merge(float* bodies, int bodyOne, int bodyTwo, int dimentions) {
    for (int i = 0; i < dimentions; ++i) {
      position(body(bodies, dimentions, bodyOne))[i] = 
          (position(body(bodies, dimentions, bodyOne))[i] * mass(body(bodies, dimentions, bodyOne)) + 
           position(body(bodies, dimentions, bodyTwo))[i] * mass(body(bodies, dimentions, bodyTwo))) / 
          (mass(body(bodies, dimentions, bodyOne)) + mass(body(bodies, dimentions, bodyTwo)));

      velocity(body(bodies, dimentions, bodyOne), dimentions)[i] = 
          (velocity(body(bodies, dimentions, bodyOne), dimentions)[i] * mass(body(bodies, dimentions, bodyOne)) + 
           velocity(body(bodies, dimentions, bodyTwo), dimentions)[i] * mass(body(bodies, dimentions, bodyTwo))) / 
          (mass(body(bodies, dimentions, bodyOne)) + mass(body(bodies, dimentions, bodyTwo)));
    }

    mass(body(bodies, dimentions, bodyOne)) += mass(body(bodies, dimentions, bodyTwo));
    mass(body(bodies, dimentions, bodyTwo)) = 0;

    radius(body(bodies, dimentions, bodyOne)) = 
        powf(powf(radius(body(bodies, dimentions, bodyOne)), dimentions) + 
        powf(radius(body(bodies, dimentions, bodyTwo)), dimentions), 1.0/dimentions);
    radius(body(bodies, dimentions, bodyTwo)) = 0;
  }

  __global__ void mergeBodies(float* bodies, int bodiesNr, int dimentions, int *close) {
    int bodyNr = blockIdx.x * blockDim.x + threadIdx.x;
    if (bodyNr >= bodiesNr) {
      return;
    }
    if (close[bodyNr] != bodyNr) {
      return;
    }

    for (int i = bodyNr + 1; i < bodiesNr; ++i) {
      if (close[i] == bodyNr) {
        merge(bodies, bodyNr, i, dimentions);
      }
    }
  }
}